import { useReducer, Reducer, useCallback } from 'react'

export type unloaded<T> = {
  kind: 'unloaded'
} & { cached?: cache<T> }

export type loading<T> = {
  kind: 'loading'
} & { cached?: cache<T> }

export type loaded<T> = {
  kind: 'loaded'
  status: number
  result: T
} & { cached?: cache<T> }

export type error<T> = {
  kind: 'error'
  status: number
  statusText: string
} & { cached?: cache<T> }

export type cache<T> = {
  timestamp: Date,
  cache: Omit<loaded<T>, 'cached'>
}

export type result<T> = loaded<T> | error<T> | unloaded<T> | loading<T>

type State<T> = result<T>

type Action<T> = {
  type: result<T>['kind']
  options?: options<T>
  payload: State<T>
}

type options<T> = {
  initialState?: result<T>
  cache?: true
  debug?: true
}

const reducer = <T>(state: State<T>, action: Action<T>): State<T> => {
  const cached: cache<T> | undefined = action.options && action.options.cache && state.kind === 'loaded' ? { timestamp: new Date(), cache: { kind: state.kind, status: state.status, result: state.result } } : state.cached

  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupCollapsed(`[ppwrapper]: State changed (${state.kind} -> ${action.payload.kind})`)
  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupCollapsed(`[ppwrapper]: Previous state:`)
  action.options && action.options.debug && state.kind !== action.payload.kind && console.info(state)
  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupEnd()
  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupCollapsed(`[ppwrapper]: Next state:`)
  action.options && action.options.debug && state.kind !== action.payload.kind && console.info(action.payload)
  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupEnd()
  action.options && action.options.debug && state.kind !== action.payload.kind && console.groupEnd()
  
  switch (action.type) {
    case 'unloaded':
      return { ...action.payload, cached: cached }
    case 'loading':
      return { ...action.payload, cached: cached }
    case 'loaded':
      return { ...action.payload, cached: cached }
    case 'error':
      return { ...action.payload, cached: cached }
    default:
      return state
  }
}

/**
 *  Performs the request.
 *
 * @typeParam T - Type expected from the `fetch`
 * @param {() => Promise<Response>} re - The `fetch` function wrapped in a lambda
 * @param {React.Dispatch<Action<T>>} dispatch - Dispatch function from the reducer
 * @param {options<T>} options - Object with options
 * @returns Returns a `Promise` which, if resolved successfuly, returns the `T` wrapper in `result<T>`
 * @public
 */
const request = async <T>(re: () => Promise<Response>, dispatch: React.Dispatch<Action<T>>, options?: options<T>): Promise<result<T>> => {
  options && options.debug && console.groupCollapsed(`[ppwrapper]: Request started`)
  options && options.debug && console.time(`[ppwrapper]: Request duration`)

  dispatch({ type: 'loading', options: options, payload: { kind: 'loading' } })

  return await re().then<result<T>>(async r => {
      if (r.ok) {
        let result: T
        let payload: loaded<T>
        try {
          result = await r.json()
          payload = {
            kind: 'loaded',
            status: r.status,
            result
          }
        } catch {
          result = 'Response yielded no JSON' as unknown as T
          payload = {
            kind: 'loaded',
            status: r.status,
            result: result as unknown as T
          }
        }
        
        options && options.debug && console.info(`[ppwrapper]: Request resolved`)
        options && options.debug && console.timeEnd(`[ppwrapper]: Request duration`)
        options && options.debug && console.groupEnd()
        
        dispatch({
          type: 'loaded', options: options, payload
        })
        
        return payload
      } else {
        const payload: error<T> = {
          kind: 'error',
          status: r.status,
          statusText: r.statusText
        }

        options && options.debug && console.warn(`[ppwrapper]: Request resolved with error`)
        options && options.debug && console.timeEnd(`[ppwrapper]: Request duration`)
        options && options.debug && console.groupEnd()
        dispatch({ type: 'error', options: options, payload })
        return payload      
      }
    }).catch(err => {
      const payload: error<T> = {
        kind: 'error',
        status: -1,
        statusText: String(err)
      }

      options && options.debug && console.warn(`[ppwrapper]: Request resolved with error`)
      options && options.debug && console.timeEnd(`[ppwrapper]: Request duration`)
      options && options.debug && console.groupEnd()
      dispatch({ type: 'error', options: options, payload })
      return payload      
    })
}

/**
 *  Initializes the state and function to perform the fetch.
 *
 * @typeParam T - Type expected from the `fetch`
 * @typeParam K - Signature of the fetch wrapper in a lambda
 * @param {K} re - The `fetch` function wrapped in a lambda
 * @param {options<T>} options - Object with options
 * @returns Returns an array with the state and callback to perform the request
 * @public
 */
export const ppwrapper = <T, K extends (...args: any[]) => Promise<Response>>(re: K, options?: options<T>): Readonly<[result<T>, (...args: Parameters<K>) => Promise<result<T>>]> => {
  const IS: State<T> = options && options.initialState ? { ...options.initialState } : { kind: 'unloaded' }

  const [ state, dispatch ] = useReducer<Reducer<State<T>, Action<T>>>(reducer, IS)

  let callback = (...args: Parameters<K>) => request<T>(() => re(...args), dispatch, options)

  const memoizedCallback = useCallback(callback, [])

  return [ state, memoizedCallback ] as const
}
