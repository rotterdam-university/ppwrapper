import { ppwrapper, result, loaded, loading, error, unloaded } from './request'
import Suspense from './suspense'

export { result, loaded, loading, error, unloaded }
export { Suspense }
export default ppwrapper