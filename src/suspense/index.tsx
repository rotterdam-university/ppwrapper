import React from 'react'
import { result, loaded, loading, error, unloaded } from "../request"

type SuspenseProps<T> = {
  unloadedComp?: (state: unloaded<T>) => JSX.Element
  loadingComp?: (state: loading<T>) => JSX.Element
  errorComp?: (state: error<T>) => JSX.Element
}

let Suspense = function<T>(state: result<T>, components?: SuspenseProps<T>): (loadedComponent: (state: loaded<T>) => JSX.Element) => JSX.Element {
  return loadedComponent => {
    if (components) {
      switch (state.kind) {
        case 'unloaded':
          return components.unloadedComp ? components.unloadedComp(state) : <></>
        case 'loading':
          return components.loadingComp ? components.loadingComp(state) : <></>
        case 'loaded':
          return loadedComponent(state)
        case 'error':
          return components.errorComp ? components.errorComp(state) : <></>
        default:
          return <></>
      }
    } else {
      if (state.kind === 'loaded')
        return loadedComponent(state)
      return <></>
    }
  }
}

export default Suspense