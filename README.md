# ppwrapper

Stay safe. Use a wrapper for your **P**rogrammable **P**romise.

## Installation

Install the package with `npm i @ruas/ppwrapper` or `yarn add @ruas/ppwrapper`

## Quick start

```tsx
const f = () => fetch('https://randomuser.me/api/')

let [ state, re ] = ppwrapper<PersonApi, typeof f>(f)
```

If you'd like to perform the request on render, pass the function to `useEffect`:

```tsx
useEffect(() => {
  re()
}, [re])
```

## Documentation
Check the wiki for documentation and examples: [ https://gitlab.com/rotterdam-university/ppwrapper/-/wikis]( https://gitlab.com/rotterdam-university/ppwrapper/-/wikis)

## Developing locally

1. Clone the package with `git clone git@gitlab.com:rotterdam-university/ppwrapper.git`
2. Install the dependencies with `yarn`
3. Link the package with `yarn link`
4. Go to `ppwrapper-docs` or any project where you want to use the wrapper and do `yarn link @ruas/ppwrapper`
5. Run `yarn start` to compile the package everytime you save a file

## Publishing to NPM

Before you publish the package, make sure you've logged in with `yarn login`. You only have to do this once.

1. Update the version in `package.json` following [semver](https://semver.org/) ([Cheatsheet](https://devhints.io/semver)). Do this in the `develop` branch.
2. It speaks for itself that the `develop` branch has to be tested and confirmed that the package works as expected.
3. Push only the version bump in the `package.json` to the **remote** `develop` branch with as descripton:

```md
Bumped version to vX.X.X
```

The `X.X.X` should correspond with the version in the `package.json`.

At this point, you can make a [merge request](https://gitlab.com/rotterdam-university/ppwrapper/-/compare/master...develop) on **Gitlab** from `develop` → `master`. Once it's approved by another developer, you can continue.

1. Pull the changes on the master branch on you local machine.
2. Create a tag **on the `master` branch** with the same version as in the `package.json` with: `git tag -a <VERSION>`
3. The message should be a short description of what's included since the last tag:

```md
Added:
• x

Fixed:
• x
```

4. Build the project with `yarn build`
5. Pushed the tag to Gitlab with: `git push --tags`
6. Publish the package with: `yarn publish --access public`. Hit enter when yarn asks: `question New version:`
